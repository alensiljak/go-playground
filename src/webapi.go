/*
Restful API
*/
package main

import (
	"fmt"
	"log"
	"net/http"
)

func homePage(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Welcome to the HomePage!")
	fmt.Println("Endpoint Hit: homePage")
}

func api(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "this is the API")
	println("API endpoint hit.")
}

func handleRequests() {
	http.HandleFunc("/", homePage)
	http.HandleFunc("/api", api)

	print("Server starting")
	log.Fatal(http.ListenAndServe(":10000", nil))
}

func main() {
	handleRequests()
}
